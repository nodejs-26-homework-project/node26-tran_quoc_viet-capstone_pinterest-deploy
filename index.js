const express = require("express");
const configs = require("./src/config");
const { handleErrors } = require("./src/helpers/error");
const { sequelize } = require("./src/models");
const v1 = require("./src/routers/v1");

const app = express();
app.use(express.json());
app.use(express.static("."))

sequelize.sync({alter: true})

app.use("/api/v1", v1);

//test deploy respone
app.use("/test", (req, res, next) => { res.status(200).json('TESTTT  OooKkkkk') });

app.use(handleErrors)

app.use("/", (req, res, next) => { res.status(200).json('Deployyy OooKkkkk') });
app.listen(configs.PORT);

// // Call these functions to generate testing data. Disable relation models and recreate tables before generating data.
// const generateDataToTest = require("./helpers/generateDataToTest");
// generateDataToTest.users();
// generateDataToTest.pictures()
// generateDataToTest.comments()

// module.exports = app;